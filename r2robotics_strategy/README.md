## Gérer les stratégies

### Fichiers

Dans le package **r2robotics_strategy**
- `launch/main_robot.launch` : lance TOUT le gros robot
- `launch/little_robot.launch` : lance TOUT le petit robot
- `strategy.py` : définit la machine à état (MAE) de la stratégie
- `states.py` : définit certains états de la MAE
- `main_robot_states.py` : définit des états de la MAE pour le gros robot
- `little_robot_states.py` : définit des états de la MAE pour le petit robot
- `actions.py` : définit les classes Action, MetaAction et ActionList
- `robot_goals.py` : définit de manière générale la classe RobotGoals qui gère l'enchaînement des actions
- `main_robot_goals.py` : définit des méthodes supplémentaires pour RobotGoals, contient la *vraie* stratégie du gros robot
- `little_robot_goals.py` : définit des méthodes supplémentaires pour RobotGoals, contient la *vraie* stratégie du petit robot

Dans le package **r2robotics_trajectory** (pas le package de ce README donc)
- `launch/graph_nodes_main_robot.launch` : définit les points et arêtes du graphe que va utiliser le gros robot pour se déplacer
- `launch/graph_nodes_little_robot.launch` : définit les points et arêtes du graphe que va utiliser le petit robot pour se déplacer

### Choisir la stratégie (homologation, qualif ...)

Dans `strategy.py`, dans la fonction `init_strat`, pour changer la stratégie utilisée, il faut changer la ligne 
```
self.type = Strat.HOMOLOGATION
```
Les différentes stratégie possibles sont :
- `Strat.HOMOLOGATION_AVOIDANCE`
- `Strat.HOMOLOGATION`
- `Strat.QUALIF`
- `Strat.FINALE`
- `Strat.TEST_TABLE`
- `Strat.DEMO`

Elles sont définies dans `robot_goals.py`.

### Changer l'enchaînement des actions pour une certaine stratégie

Dans `main_robot_goals.py` ou `little_robot_goals.py`, chaque méthode définit l'enchaînement des actions pour une certaine stratégie (homologation, qualif ...). La liste des actions sera effectuée dans l'ordre par le robot.

Pour aller à un point, il faut son ID (voir section suivante). A priori, chaque point est défini pour chacune des couleur, et finit par `_vert` ou `_orange` (c'est pour ça qu'on ajoute `col` à la fin de l'ID) :
```
self.add_goto(pointID="premier_point"+col)
```

### Définir le graphe de déplacement

Dans les fichiers `launch/graph_nodes_main_robot.launch` et `launch/little_nodes_main_robot.launch` du package **r2robotics_trajectory**, les graphes sont définis. Au début du fichier launch, on indique le nombre total de point, ainsi que d'edge arc-de-cercle définis manuellement (uniquement les arc de cercle comptent là dedans !).
```
<param name="number_graph_edges" value="0" type="int" />
<param name="number_graph_points" value="2" type="int" />
```
Chaque point est identifié dans le fichier launch pour un numéro, de `0` à `number_graph_points-1`. Ce numéro ne sert qu'au sein du fichier launch et ne représente rien d'autre, par contre chaque point a un numéro **unique** et aussi **attention aux typos** ! Pour chaque point, on définit :
- un `id` : qui est utilisé par la stratégie (voir section précédente)
- `x` : coordonnée x (entre 0 et 2000, en mm)
- `y` : coordonnée y (entre 0 et 3000, en mm)
- soit un `yaw` en dégrés, soit un bool `yaw_needed` à `false` : si c'est à `false`, le robot ne s'orientera pas spécialement en arrivant à ce point et gardera son cap précédent
- un entier `number_neighbors` : le nombre d'autres points reliés par des rotations/lignes droites à ce point
- pour chaque voisin, son id : `neighbor_i` avec i entre 0 et le `number_neighbors - 1`
- pour certains voisins, si on désire que la transition soit unidirectionelle : `neighbor_i_unidirectional` à `true` si on veut que le robot ne puisse aller que de ce point vers son voisin, mais pas du voisin vers ce point

Dans ce fichier, on peut aussi définir des arcs de cercle en tant que `edge`.

## JE VEUX CHANGER UN PARAMETRE, C'EST OU ?

### IMPORTANT

- positions initiales : dans les fichiers `main_robot_parameters.launch` et `little_robot_parameters.launch` du package `r2robotics_strategy` (en mm). Penser à changer les points "start" du graphe.
- paramètres d'évitement : distance d'arrêt, distance de reprise, marge : fichier `main_robot_sonars.launch` et `little_robot_sonars.launch` du package `r2robotics_avoidance` (en mm)
- vitesse max : dans le fichier `strategy.py` du package `r2robotics_strategy`, dans la fonction `init_max_speeds` (pour l'instant on est à 0.3 m/s en HOMOLOGATION et 0.6 m/s pour les autres strats) (pour l'instant on ne peux pas changer la vitesse max entre deux mouvements, mais c'est possible)
- quelle strat on utilise : dans le fichier `strategy.py` du package `r2robotics_strategy`, dans l'appel `self.init_strat("HOMOLOGATION")`, changer HOMOLOGATION en QUALIF
- la hauteur des bras pour pousser l'abeille : dans le fichier `little_robot_states.py` du package `r2robotics_strategy`, LeftBeeArmStatus et RightBeeArmStatus représente les valeurs (presque en degrés, et attention : les 2 servos ne sont pas dans le même sens), les valeurs actuelles correspondent pas trop mal mais il faut peut-être ajuster la valeur de HORIZONTAL pour pousser l'abeille

### Moins utile

- si le robot est omnidirectionel (gros robot) ou non (petit robot) : dans les launcher `main_robot_launcher.launch` et `little_robot_launcher.launch` du package `r2robotics_trajectory`, et probablement quelque part dans le package `r2robotics_control`
- si on utilise la stratégie du petit robot ou du gros robot : dans les launcher `main_robot_strategy.launch` et `little_robot_strategy.launch` du package `r2robotics_strategy`
- fréquence du noeud stratégie : dans les launcher `main_robot_strategy.launch` et `little_robot_strategy.launch` du package `r2robotics_strategy`
- fréquence du noeud trajectoire : dans les launcher `main_robot_launcher.launch` et `little_robot_launcher.launch` du package `r2robotics_trajectory`
- utilisation de Gazebo ou non : dans le launcher `simulator.launch` du package `r2robotics_simulator`

## How to

### Lancer les programmes des robots

Pour lancer le programme sur le vrai robot : voir section "Pi - Lancement". Normalement, en match, la Pi se démarre seule et le programme du robot se lance tout seul.

A l'allumage, le robot va s'initialiser. L'initialisation prend jusqu'à 30 secondes environ et sa fin est visualisée par un mouvement du robot :
- gors robot : les tourelles tournent à 90 puis 0 puis 90 degrés
- petit robot : le bras de la bonne couleur, côté bord de table (gauche si orange, droit si vert) monte à 90° puis redescend
Si l'initialisation n'a pas lui au bout d'une demi-seconde, il faut s'inquiéter. Redémarrer le robot, en faisant attention à ouvrir l'ARU en premier.

Il faut ensuite enfoncer le starter, puis l'enlever, et le robot se lance.

### Pour une simulation

Afin de simuler le lancement du robot, il faut envoyer les messages suivants. Il faut donc envoyer deux fois un message sur le topic `/bumpers_feedback` (le premier pour signaler que le topic `\bumpers_feedback` est bien publié, le deuxième pour signaler que le start n'est pas dans le bumper). L'initialisation a lieu après l'envoi du premier message.
```
rostopic pub -1 /bumpers_feedback r2robotics_msgs/Bumpers "{bumpers:[true, true]}"
rostopic pub -1 /bumpers_feedback r2robotics_msgs/Bumpers "{bumpers:[true, true]}"
```
L'élément d'indice 0 correspond au bumper du starter, celui d'indice 1 au bouton de choix de couleur. Il faut ensuite simuler l'enlèvement du starter (et choix des stratégie et de la couleur) :

Pour VERT :
```
rostopic pub -1 /bumpers_feedback r2robotics_msgs/Bumpers "{bumpers:[false, false]}"
```
Pour ORANGE :
```

rostopic pub -1 /bumpers_feedback r2robotics_msgs/Bumpers "{bumpers:[false, true]}"
```

Pour simuler que le robot a atteint son waypoint (chaque trajectoire est divisé en waypoint) :
```
rosservice call /trajectory_as_if_done "{}"
```

Divers services sont utilisés entre la stratégie, la trajectoire et l'asserv et peuvent être utilisés pour simuler le comportement du robot.

### Si un noeud plante

Rajouter ce champ dans le tag node du fichier launch :

```
launch-prefix="gdb -ex run --args"
```

### A installer

```
sudo apt-get install ros-kinetic-rosserial
```


## La Pi

### Quiiiiid ?

Le login de chacune des Pi est `pi`. Le mot de passe est le même pour les deux. Elle se connecte automatiquement au réseau Ponyville. L'adresse IP est à regarder pour se connecter. La Pi de la PMI possède un fichier dans le home `CECI_EST_LA_PMI.txt`. Le même sera mis sur la Pi du gros robot.


### Compilation

Les subrepo ne sont plus sur les Pi (heureusement). chaque dossier dans `r2robotics_main` est un repo indépendant, utilisation normale. Quelques scripts sont disponibles pour checker les git status, pour récupérer le code, et pour compiler (`pi_compilation.bash`). 

Pour compiler sur la Pi une fois avoir récupéré le code du git, faite attention à ce que :
- la Teensy ne soit pas branché (ça pompe du jus -> + lent)
- ros ne soit pas en train de tourné ('rostopic list' et si c'est lancé --> 'sudo service r2robotics stop')

### Lancement

**La Pi est configurée pour lancer automatiquement** tout le programme du robot (ie, lancer le roslaunch `main_robot.launch` ou `little_robot.launch` du package `r2robotics_strategy`).

Inconvénient : pour tester/débugger, il n'y a pas d'affichage ! Il vaut mieux donc arrêter le programme lancé automatique et le relancer manuellement, pour avoir tous les bons affichages pour débugger. Pour travailler / compiler, il est conseillé aussi d'arrêter le programme pour ne pas ralentir la Pi. Pour cela, une fois la Pi allumée et en ssh :
```
sudo service r2robotics stop
```

Pour lancer le programme du robot avec les affichages : 
```
roslaunch r2robotics_strategy main_robot.launch
roslaunch r2robotics_strategy little_robot.launch
```
Ou `PMI_LAUNCH` ou `GR_LAUNCH` (alias équivalent aux lignes du dessus).

Pour changer la configuration de lancement automatique et lancer un autre launch file au démarrage de la Pi :
```
rosrun robot_upstart install r2robotics_strategy/launch/new_launch.launch
```
