## ! DO NOT MANUALLY INVOKE THIS setup.py, USE CATKIN INSTEAD

from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

# fetch values from package.xml
setup_args = generate_distutils_setup(
    packages=['r2robotics_strategy'],
    package_dir={'': 'src'},
    requires=['std_msgs', 'rospy', 'r2robotics_msgs', 'r2robotics_srvs', 'r2robotics_PR_base', 'smach']
)

setup(**setup_args)
