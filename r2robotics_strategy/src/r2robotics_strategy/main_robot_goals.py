#!/usr/bin/env python

import roslib
import rospy
from enum import Enum
from r2robotics_strategy.point import Point
from r2robotics_strategy.utils import *
from r2robotics_strategy.actions import *
from r2robotics_strategy.robot_goals import *
from r2robotics_strategy.main_robot_goals import *

def init_homologation_avoidance(self):
    nb_aller_retour = 10
    if self.color == Color.GREEN:
        for k in range(nb_aller_retour):
            self.actions.add_goto(pointID="milieu_vert")
            self.actions.add_goto(pointID="milieu_orange")
    if self.color == Color.ORANGE:
        for k in range(nb_aller_retour):
            self.actions.add_goto(pointID="milieu_orange")
            self.actions.add_goto(pointID="milieu_vert")
RobotGoals.init_homologation_avoidance = init_homologation_avoidance

def init_homologation(self):
    self.actions.add_wait(10)
    if self.color == Color.GREEN:
        col = "vert"
        other_col = "orange"
    elif self.color == Color.ORANGE:
        col = "orange"
        other_col = "vert"
    self.actions.add_goto(pointID="pousser_cubes_"+col)
    self.actions.add_goto(pointID="zone_construction_"+col)
    # TODO : verifier que l'evitement marche bien avec les points suivants
    # ie qu'il y a des sonars bien orientes devant qui reperent bien
    # sinon, changer l'orientation des points pour que ca evite bien
    # self.actions.add_goto(pointID="milieu_"+col) 
    # self.actions.add_goto(pointID="milieu_"+other_col)
    # self.actions.add_goto(pointID="milieu_"+col) 
RobotGoals.init_homologation = init_homologation

# TODO
def init_qualif(self):
    if self.color == Color.GREEN:
        col = "vert"
    elif self.color == Color.ORANGE:
        col = "orange"
    
    # TODO :
    # definir les points dans le graphe et noter ici l'enchainement
    # pour attraper les cubes, les deposer dans la zone de construction,
    # et appuyer l'interrupteur
    # Il faut probablement attendre un petit peu que la PMI sorte
    # Il faut probablement aussi faire attention

    # Pour la qualif, ca vaut surement le coup d'augmenter la vitesse max
    # (  )
    # 
    #self.actions.add_gr_actuator(actuator="interrupteur_arm", value="LOW")
    self.actions.add_gr_actuator(actuator="interrupteur_arm", value="LOW")
    self.actions.add_wait(15)
    self.actions.add_goto(pointID="replace_rotation_pre_cubes_"+col)
    # self.actions.add_goto(pointID="get_cubes_from_y_"+col)
    # self.actions.add_goto(pointID="put_cubes_in_zone_"+col)
    # self.actions.add_goto(pointID="leave_zone_"+col)
    self.actions.add_goto(pointID="translate_to_switch_"+col)
    self.actions.add_gr_actuator(actuator="interrupteur_arm", value="HIGH")
    self.actions.add_goto(pointID="switch_"+col)
    self.actions.add_gr_actuator(actuator="interrupteur_arm", value="LOW")
    self.actions.add_goto(pointID="leave_switch_"+col)

RobotGoals.init_qualif = init_qualif

def init_test_table(self):
    if self.color == Color.ORANGE:
        pass
RobotGoals.init_test_table = init_test_table

def init_finale(self):
    pass
RobotGoals.init_finale = init_finale

def init_demo(self):
        pass
RobotGoals.init_demo = init_demo