#!/usr/bin/env python

import roslib
import rospy
from smach import *
from smach_ros import SimpleActionState

from r2robotics_strategy.utils import *
from r2robotics_strategy.actions import *
from r2robotics_strategy.robot_goals import *
from r2robotics_strategy.little_robot_states import *
from r2robotics_strategy.main_robot_states import *

from r2robotics_msgs.msg import Movement
from r2robotics_msgs.msg import RobotPosition
from r2robotics_msgs.msg import ServosPWM_MSG, Bumpers
from r2robotics_msgs.msg import SonarArray
from std_srvs.srv import SetBool
from r2robotics_srvs.srv import move as moveSrv
from r2robotics_srvs.srv import set_direction, motors_disable, activate
from r2robotics_srvs.srv import set_position
from r2robotics_srvs.srv import set_direction
from r2robotics_srvs.srv import stopControl
from r2robotics_srvs.srv import deactivate
from r2robotics_srvs.srv import setControlMaxLinearSpeed
from r2robotics_actions.msg import TargetPointAction, TargetPointGoal
from r2robotics_actions.msg import ConsigneAction, ConsigneGoal


################################################
### Main states for the global state machine ###
################################################

class InitWaitState(State):
    def __init__(self, x_init, y_init, theta_init):
        State.__init__(self, outcomes=['succeeded','aborted', 'preempted'])
        self.x_init = x_init
        self.y_init = y_init
        self.theta_init = theta_init

    def execute(self, userdata):
        STRAT_LOG( "Entering 'Initialization' state" )

        # Wait before calling!!
        # rospy.wait_for_service('activate')
        # activate_servo = rospy.ServiceProxy('activate', activate )

        #rospy.sleep(2.) # Maybe...
        # activate_servo(idServo=254)

        # rospy.wait_for_service('trajectory_go_to')
        # rospy.wait_for_service('trajectory_do_path')
        # rospy.wait_for_service('trajectory_do_relative_path')
        rospy.wait_for_service('trajectory_set_position')
        self.trajectory_set_position_srv = rospy.ServiceProxy('trajectory_set_position', set_position )
        self.trajectory_set_position_srv(x=self.x_init,y=self.y_init,theta=self.theta_init)
        rospy.wait_for_service('setDirection')

        rospy.sleep(1.)
        return 'succeeded'


class TestActuators(State):
    def __init__(self, stratmaster):
        State.__init__(self, outcomes=['succeeded', 'aborted', 'preempted'])
        self.stratmaster = stratmaster
        self.servos_pub = rospy.Publisher('servos_PWM', ServosPWM_MSG, queue_size=10)

    def execute(self, userdata):
        if self.stratmaster.MAIN_ROBOT:
            rospy.wait_for_service('setDirection')
            self.set_direction_srv = rospy.ServiceProxy('setDirection', set_direction )
            self.set_direction_srv(angle=90.0)
            rospy.sleep(0.8)
            self.set_direction_srv(angle=0.0)
            rospy.sleep(0.8)
            self.set_direction_srv(angle=90.0)
            rospy.sleep(0.8)
            STRAT_LOG("ROUES TOURNEES")
            rospy.wait_for_service('move')
            self.move = rospy.ServiceProxy('move', moveSrv )
            if self.stratmaster.color == Color.GREEN:
                self.move(idServos=[101], angles=[VERT_BRAS_OUVERT], times=[10])
                self.move(idServos=[100], angles=[ORANGE_BRAS_FERME], times=[10])
            if self.stratmaster.color == Color.ORANGE:
                self.move(idServos=[100], angles=[ORANGE_BRAS_OUVERT], times=[10])
                self.move(idServos=[101], angles=[VERT_BRAS_FERME], times=[10])
            STRAT_LOG("PETITS PUSH PUSH INTERRUPTEUR MIS EN AVANT !")
            return 'succeeded'

        elif self.stratmaster.LITTLE_ROBOT:
            self.servos_pub.publish( bras_abeille_gauche=LeftBeeArmStatus['INSIDE'],
                                        bras_abeille_droite=RightBeeArmStatus['INSIDE'] )
            rospy.sleep(1)
            if self.stratmaster.color == Color.GREEN:
                self.servos_pub.publish( bras_abeille_gauche=LeftBeeArmStatus['INSIDE'],
                                        bras_abeille_droite=RightBeeArmStatus['HORIZONTAL'] )
            if self.stratmaster.color == Color.ORANGE:
                self.servos_pub.publish( bras_abeille_gauche=LeftBeeArmStatus['HORIZONTAL'],
                                        bras_abeille_droite=RightBeeArmStatus['INSIDE'] )
            return 'succeeded'


class SendInitialControl(State):
    def __init__(self, stratmaster, x_init, y_init):
        State.__init__(self, outcomes=['succeeded', 'aborted', 'preempted'])
        self.global_control_topic = rospy.Publisher('global_control', Movement, queue_size=10)
        self.set_stop = rospy.ServiceProxy( 'trajectory_set_error_stop', SetBool )
        rospy.wait_for_service('move')
        self.move = rospy.ServiceProxy('move', moveSrv )
        rospy.wait_for_service('deactivate')
        self._x = x_init/1000.0
        self._y = y_init/1000.0
        self.stratmaster = stratmaster
        self.servos_pub = rospy.Publisher('servos_PWM', ServosPWM_MSG, queue_size=10)

    def execute(self, userdata):
        if self.stratmaster.LITTLE_ROBOT:
            self.servos_pub.publish( bras_abeille_gauche=LeftBeeArmStatus['INSIDE'],
                                        bras_abeille_droite=RightBeeArmStatus['INSIDE'] )
            STRAT_LOG("BRAS ABEILLE RENTRE !")
        if self.stratmaster.MAIN_ROBOT:
            self.move(idServos=[101], angles=[VERT_BRAS_FERME], times=[10])
            self.move(idServos=[100], angles=[ORANGE_BRAS_FERME], times=[10])
            STRAT_LOG("PETITS PUSH PUSH RENTRES !")
        self.global_control_topic.publish(mode=0, rotation_center_position=[0,0], position_to_reach=[self._x,self._y])
        rospy.sleep(1)
        self.set_stop(data=False)
        return 'succeeded'

class InitControl(State):
    def __init__(self, stratmaster):
        State.__init__(self, outcomes=['succeeded', 'failed'])
        self.stratmaster = stratmaster

    def execute(self, userdata):
        try:
            self.stratmaster.init_color()
        except:
            return 'failed'
        try:
            self.stratmaster.init_start_position()
        except:
            return 'failed'
        try:
            self.stratmaster.init_max_speeds()
        except:
            return 'failed'
        try:
            if self.stratmaster.MAIN_ROBOT:
                rospy.wait_for_service('move')
                self.move = rospy.ServiceProxy('move', moveSrv )
                self.move(idServos=[101], angles=[VERT_BRAS_FERME], times=[10])
                self.move(idServos=[100], angles=[ORANGE_BRAS_FERME], times=[10])
                STRAT_LOG("PETITS PUSH PUSH RENTRES !")
        except:
            return 'failed'
        if self.stratmaster.LITTLE_ROBOT:
            self.servos_pub = rospy.Publisher('servos_PWM', ServosPWM_MSG, queue_size=10)
            self.servos_pub.publish( bras_abeille_gauche=LeftBeeArmStatus['INSIDE'],
                                        bras_abeille_droite=RightBeeArmStatus['INSIDE'] )
            STRAT_LOG("BRAS ABEILLE RENTRE !")
        return 'succeeded'

class SetStratAndColor(State):
    def __init__(self, stratmaster):
        State.__init__(self, outcomes=['succeeded', 'failed'])
        self.stratmaster = stratmaster

    def execute(self, userdata):
        try:
            self.stratmaster.init_color()
        except:
            return 'failed'
        try:
            self.stratmaster.init_start_position()
        except:
            return 'failed'
        try:
            self.stratmaster.init_max_speeds()
        except:
            return 'failed'
        try:
            self.stratmaster.goals.set_strat(self.stratmaster.type)
        except:
            return 'failed'
        return 'succeeded'

class InitActuators(State):
    def __init__(self, stratmaster):
        State.__init__(self, outcomes=['succeeded', 'failed'])
        self.stratmaster = stratmaster

    def execute(self, userdata):
        if self.stratmaster.LITTLE_ROBOT:
            self.servos_pub = rospy.Publisher('servos_PWM', ServosPWM_MSG, queue_size=10)
            self.servos_pub.publish( bras_abeille_gauche=LeftBeeArmStatus['INSIDE'],
                                        bras_abeille_droite=RightBeeArmStatus['INSIDE'] )
            STRAT_LOG("BRAS ABEILLE RENTRE !")
        # try:
        #     if self.stratmaster.MAIN_ROBOT:
        #         rospy.wait_for_service('move')
        #         self.move = rospy.ServiceProxy('move', moveSrv )
        #         if self.stratmaster.color == Color.GREEN:
        #             self.move(idServos=[101], angles=[-93], times=[10])
        #             self.move(idServos=[100], angles=[-33], times=[10])
        #         if self.stratmaster.color == Color.ORANGE:
        #             self.move(idServos=[100], angles=[-27], times=[10])
        #             self.move(idServos=[101], angles=[-33], times=[10])
        #         STRAT_LOG("PETITS PUSH PUSH INTERRUPTEUR MIS EN AVANT !")
        # except:
        #     return 'failed'
        return 'succeeded'


class SetLinearSpeed(State):
    def __init__(self, max_speed):
        State.__init__(self, outcomes=['succeeded', 'aborted'])
        rospy.wait_for_service('set_max_linear_speed')
        self.set_max_linear_speed_srv = rospy.ServiceProxy('set_max_linear_speed', setControlMaxLinearSpeed)
        self.max_speed = max_speed

    def execute(self, userdata):
        try:
            self.set_max_linear_speed_srv(speed=self.max_speed)
        except:
            return 'aborted'
        return 'succeeded'

class ReadyForStart(State):
    def __init__(self):
        State.__init__(self, outcomes=['ready_done','ready_failed'])

    def execute(self, userdata):
        STRAT_LOG( "Entering 'ReadyFortStart' state" )
        return 'ready_done'


class End(State):
    def __init__(self):
        State.__init__(self, outcomes=['end_done','end_failed'])

    def execute(self, userdata):
        #rospy.loginfo( "Entering 'End' state" )
        rospy.sleep(1)
        return 'end_done'


class Nothing(State):
    def __init__(self):
        State.__init__(self, outcomes=['continue'])

    def execute(self, userdata):
        rospy.sleep(0.1)
        return 'continue'


class Wait(State):
    def __init__(self):
        State.__init__(self, outcomes=['continue', 'preempted'])

    def execute(self, userdata):
        #rospy.loginfo( "Entering 'Initialization' state" )
        #rospy.sleep(1.)
        for i in range(5):
            rospy.sleep(1)
            if self.preempt_requested():
                self.service_preempt()
                return 'preempted'
        return 'continue'


class StopAndDisable(State):
    def __init__(self, stratmaster):
        State.__init__(self, outcomes=['done', 'preempted'])
        self.stratmaster = stratmaster

    def execute(self, userdata):
        self.stop_control_srv = rospy.ServiceProxy('stop_control', stopControl )
        rospy.wait_for_service('stop_control')
        self.set_direction_srv = rospy.ServiceProxy( 'setDirection', set_direction )
        rospy.wait_for_service('setDirection')
        self.servos_deactivate_srv = rospy.ServiceProxy( 'deactivate', deactivate )
        rospy.wait_for_service('deactivate')
        self.servos_pub = rospy.Publisher('servos_PWM', ServosPWM_MSG, queue_size=10)
        try:
            self.stop_control_srv()
            self.set_direction_srv(angle=0) # set wheels at angle 0 in order to be able to be moved to free cubes
            self.servos_deactivate_srv(idServo=254)
            self.servos_pub.publish( bras_abeille_gauche=LeftBeeArmStatus['INSIDE'],
                                        bras_abeille_droite=RightBeeArmStatus['INSIDE'] )
            if self.stratmaster.MAIN_ROBOT:
                rospy.wait_for_service('move')
                self.move = rospy.ServiceProxy('move', moveSrv )
                self.move(idServos=[101], angles=[VERT_BRAS_FERME], times=[10])
                self.move(idServos=[100], angles=[ORANGE_BRAS_FERME], times=[10])
                STRAT_LOG("PETITS PUSH PUSH RENTRES !")
            rospy.sleep(0.1)
            pass
        except:
            pass
        if self.preempt_requested():
            self.service_preempt()
            return 'preempted'
        return 'done'


class StopAndDisable2(State):
    def __init__(self, stratmaster):
        State.__init__(self, outcomes=['done'])
        self.stop_control_srv = rospy.ServiceProxy( 'stop_control', stopControl )
        rospy.wait_for_service('stop_control')
        self.set_direction_srv = rospy.ServiceProxy( 'setDirection', set_direction )
        rospy.wait_for_service('setDirection')
        self.servos_deactivate_srv = rospy.ServiceProxy( 'deactivate', deactivate )
        rospy.wait_for_service('deactivate')
        self.servos_pub = rospy.Publisher('servos_PWM', ServosPWM_MSG, queue_size=10)
        self.stratmaster = stratmaster

    def execute(self, userdata):
        try:
            self.stop_control_srv()
            self.set_direction_srv(angle=0) # set wheels at angle 0 in order to be able to be moved to free cubes
            self.servos_deactivate_srv(idServo=254)
            self.servos_pub.publish( bras_abeille_gauche=LeftBeeArmStatus['INSIDE'],
                                        bras_abeille_droite=RightBeeArmStatus['INSIDE'] )
            if self.stratmaster.MAIN_ROBOT:
                rospy.wait_for_service('move')
                self.move = rospy.ServiceProxy('move', moveSrv )
                self.move(idServos=[101], angles=[-33], times=[10])
                self.move(idServos=[100], angles=[-33], times=[10])
                STRAT_LOG("PETITS PUSH PUSH RENTRES ! FIN !")
        except:
            pass
        return 'done'
