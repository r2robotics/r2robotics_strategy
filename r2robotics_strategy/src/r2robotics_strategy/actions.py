#!/usr/bin/env python

import roslib
import rospy
from r2robotics_strategy.point import Point
from enum import Enum

DIAMETRE_CYLINDER = 63
RAYON_CYLINDER = 31.5

DIAMETRE_ROCKET = 80
RAYON_ROCKET = 40

ROBOT_RADIUS = 146.95
ROTATION_90 = ROBOT_RADIUS * 1.5708

SLOW_SPEED = 300
NORMAL_SPEED = 700
HIGH_SPEED = 2000

class Color(Enum):
    GREEN = 0
    ORANGE = 1

class Turbine(Enum):
    ON = 100
    OFF = 0

class Action(object):
    def __init__(self):
        self.set_donothing()

    def set_donothing(self):
        self.action = "nothing"
        self.secable = True

    def copy(self, ta):
        self.action = ta.action
        self.secable = ta.secable
        if ta.action == "nothing":
            self.set_nothing()
        elif ta.action == "move":
            self.set_move(ta.distance, ta.orientation, ta.self_rotation, ta.speed)
        elif ta.action == "goto":
            try:
                self.set_goto( ta.pointID, ta.x, ta.y, ta.cap, ta.cap_needed, ta.self_rotation_only, ta.speed)
            except:
                rospy.logerror("error trying to copy action")
                print "\nCOPY :", ta.action
                print dir(ta)
                print '\n'
                #d = ta.__dict__
        elif ta.action == 'wait':
            self.set_wait( ta.wait_time_seconds )
        elif ta.action == 'recalibration':
            self.set_recalibration( ta.dx, ta.dy )
        elif ta.action == 'PMI_set_bee_arm':
            self.set_PMI_set_bee_arm( ta.bee_arm_value )
        elif ta.action == 'PMI_roue':
            self.set_PMI_roue( ta.roue, ta.roue_sens, ta.roue_iteration )
        elif ta.action == 'PMI_pushpush':
            self.set_PMI_pushpush( ta.bielle_position )
        elif ta.action == 'PMI_canon':
            self.set_PMI_canon( ta.canon_position )
        elif ta.action == 'stop_avoidance':
            self.set_stop_avoidance()
        elif ta.action == 'resume_avoidance':
            self.set_resume_avoidance()
        elif ta.action == 'gr_actuator':
            self.set_gr_actuator(ta.gr_actuator, ta.gr_actuator_value)

    def set_move(self, _distance, _orientation, _self_rotation, _speed, secable=False):
        self.distance = _distance
        self.orientation = _orientation
        self.self_rotation = _self_rotation
        self.speed = _speed
        self.action = "move"
        self.secable = secable

    def set_goto(self,  pointID=None, _x=None, _y=None, _cap=None, cap_needed=None, self_rotation=None, speed=None, secable=False):
        if speed is None:
            speed = NORMAL_SPEED
        if self_rotation is None:
            self_rotation = False
        if cap_needed is None:
            cap_needed = False
        if _x is not None and _y is not None and _cap is not None:
            self.x = _x
            self.y = _y
            self.cap = _cap
            self.pointID = "pose"
        if pointID is not None:
            self.pointID = pointID
            self.x, self.y, self.cap = None, None, None
        self.cap_needed = cap_needed
        self.self_rotation_only = self_rotation
        self.speed = speed
        self.action = "goto"
        self.secable = secable

    def set_wait(self, wait_time_seconds=0):
        self.action = 'wait'
        self.wait_time_seconds = wait_time_seconds
        self.secable = False

    def set_recalibration(self, dx=0, dy=0):
        self.action = 'recalibration'
        self.dx = dx
        self.dy = dy
        self.secable = False

    def set_stop_avoidance(self):
        self.action = 'stop_avoidance'

    def set_gr_actuator(self, actuator, value):
        self.action = 'gr_actuator'
        self.gr_actuator = actuator
        self.gr_actuator_value = value

    def set_resume_avoidance(self):
        self.action = 'resume_avoidance'

    def set_PMI_set_bee_arm(self, value):
        self.action = 'PMI_set_bee_arm'
        self.bee_arm_value = value
        self.secable = False

    def set_PMI_roue(self, roue, sens, roue_iteration ):
        self.action = 'PMI_roue'
        self.roue = roue
        self.roue_sens = sens
        self.roue_iteration = roue_iteration
        self.secable = False

    def set_PMI_pushpush(self, bielle_position):
        self.action = 'PMI_pushpush'
        self.bielle_position = bielle_position
        self.secable = False

    def set_PMI_canon(self, pos):
        self.action = 'PMI_canon'
        self.canon_position = pos
        self.secable = False

class MetaAction(object):
    def __init__(self):
        # this defines the meta-action by its subactions
        self.subactions = []
        # this is a copy of the subactions, that is modified when the meta-actions is started
        # it is reset when the meta-action is stopped (in case of avoidance)
        self.instance_subactions = []

    def add_subaction(self, act):
        self.subactions.append(act)
        self.reset()

    def init(self):
        self.subactions = []
        self.instance_subactions = []

    def reset(self):
        self.instance_subactions = self.subactions[:]

    def append(self, act):
        self.subactions.append(act)
        self.instance_subactions.append(act)

    def get_next_subaction(self):
        if(len(self.instance_subactions) > 0):
            return self.instance_subactions[0]
        else:
            return Action() #do nothing

    def is_empty(self):
        return ( len(self.instance_subactions) == 0 )

    def pop_first(self):
        return self.instance_subactions.pop(0)

    def add_move(self, _distance, _orientation, _self_rotation, _speed):
        temp = Action()
        temp.set_move( _distance, _orientation, _self_rotation, _speed )
        self.append(temp)

    def add_turn(self, _cap):
        temp = Action()
        temp.set_goto( 0, 0, _cap, True, True, NORMAL_SPEED )
        self.append(temp)

    def add_goto(self, pointID, cap_needed = None, speed = None ):
        temp = Action()
        temp.set_goto( pointID )
        self.append(temp)

    def add_wait(self, wait_time_seconds):
        temp = Action()
        temp.set_wait( wait_time_seconds )
        self.append(temp)

    def add_recalibration(self, dx, dy):
        temp = Action()
        temp.set_recalibration(dx, dy)
        self.append(temp)

    def add_stop_avoidance(self):
        temp = Action()
        temp.set_stop_avoidance()
        self.append(temp)

    def add_gr_actuator(self, actuator, value):
        temp = Action()
        temp.set_gr_actuator(actuator, value)
        self.append(temp)

    def add_resume_avoidance(self):
        temp = Action()
        temp.set_resume_avoidance()
        self.append(temp)

    def add_PMI_set_bee_arm(self, value):
        temp = Action()
        temp.set_PMI_set_bee_arm(value)
        self.append(temp)

    def add_PMI_roue(self, roue, sens, roue_iteration):
        temp = Action()
        temp.set_PMI_roue(roue, sens, roue_iteration)
        self.append(temp)

    def add_PMI_pushpush(self, pos):
        temp = Action()
        temp.set_PMI_pushpush(pos)
        self.append(temp)

    def add_PMI_canon(self, pos):
        temp = Action()
        temp.set_PMI_canon(pos)
        self.append(temp)

    def set_move(self, _distance, _orientation, _self_rotation, _speed):
        self.init()
        self.add_move( _distance, _orientation, _self_rotation, _speed )

    def set_turn(self, _cap):
        self.init()
        self.add_turn( _cap )

    def set_goto(self, pointID, cap_needed = None, speed = None ):
        self.init()
        self.add_goto( pointID, cap_needed, speed )

    def set_wait(self, wait_time_seconds):
        self.init()
        self.add_wait( wait_time_seconds )

    def set_recalibration(self, dx, dy):
        self.init()
        self.add_recalibration( dx, dy )

    def set_PMI_set_bee_arm(self, value):
        self.init()
        self.add_PMI_set_bee_arm(value)

    def set_stop_avoidance(self):
        self.init()
        self.add_stop_avoidance()

    def set_gr_actuator(self, actuator, value):
        self.init()
        self.add_gr_actuator(actuator, value)

    def set_resume_avoidance(self):
        self.init()
        self.add_resume_avoidance()

    def set_PMI_roue(self, roue, sens, roue_iteration):
        self.init()
        self.add_PMI_roue(roue, sens, roue_iteration)

    def set_PMI_pushpush(self, pos):
        self.init()
        self.add_PMI_pushpush(pos)

    def set_PMI_canon(self, pos):
        self.init()
        self.add_PMI_canon(pos)


class ActionList(object):
    def __init__(self):
        self.actions = []

    def size(self):
        return len(self.actions)

    def append(self, _ta):
        self.actions.append(_ta)

    def add_move(self, _distance, _orientation, _self_rotation, _speed):
        temp = MetaAction()
        temp.set_move( _distance, _orientation, _self_rotation, _speed )
        self.append(temp)

    def add_turn(self, _cap):
        temp = MetaAction()
        temp.set_goto( 0, 0, _cap, True, True, NORMAL_SPEED )
        self.append(temp)

    def add_goto(self, pointID, cap_needed = None, speed = None ):
        temp = MetaAction()
        temp.set_goto( pointID, cap_needed )
        self.append(temp)

    def add_wait(self, wait_time_seconds):
        temp = MetaAction()
        temp.set_wait( wait_time_seconds )
        self.append(temp)

    def add_recalibration(self, dx, dy):
        temp = MetaAction()
        temp.set_recalibration(dx, dy)
        self.append(temp)

    def add_stop_avoidance(self):
        temp = MetaAction()
        temp.set_stop_avoidance()
        self.append(temp)

    def add_gr_actuator(self, actuator, value):
        temp = MetaAction()
        temp.set_gr_actuator(actuator, value)
        self.append(temp)

    def add_resume_avoidance(self):
        temp = MetaAction()
        temp.set_resume_avoidance()
        self.append(temp)

    def add_PMI_set_bee_arm(self, value):
        temp = MetaAction()
        temp.set_PMI_set_bee_arm(value)
        self.append(temp)
        
    def add_PMI_roue(self, roue, sens, roue_iteration):
        temp = MetaAction()
        temp.set_PMI_roue(roue, sens, roue_iteration)
        self.append(temp)

    def add_PMI_pushpush(self, pos):
        temp = MetaAction()
        temp.set_PMI_pushpush(pos)
        self.append(temp)

    def add_PMI_canon(self, pos):
        temp = MetaAction()
        temp.set_PMI_canon(pos)
        self.append(temp)


    def add_launch_balls(self):
        pass

        if(len(self.actions) > 0):
            return self.actions[0]
        else:
            return MetaAction() #do nothing

    def get_next_action(self):
        # if the current meta action is not empty
        # return its next action
        if not self.actions[0].is_empty():
            return self.actions[0].get_next_subaction()
        # if current meta action is empty, remove it and return next action again
        self.actions.pop(0)
        self.get_next_action()

    def pop_first(self):
        # pop the current meta action if it's not empty
        # then remove it if it's now empty
        if not self.actions[0].is_empty():
            res = self.actions[0].pop_first()
            if self.actions[0].is_empty:
                self.actions.pop(0)
            return res
        # if current meta action is empty, remove it and pop again
        self.actions.pop(0)
        self.pop_first()

class TableState(object):
    def __init__(self):
        pass
