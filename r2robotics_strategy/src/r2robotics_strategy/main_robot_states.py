#!/usr/bin/env python

import roslib
import rospy
from smach import *
from smach_ros import SimpleActionState
from enum import Enum

from r2robotics_strategy.utils import *
from r2robotics_strategy.actions import *

from r2robotics_msgs.msg import Movement
from r2robotics_msgs.msg import RobotPosition
from r2robotics_msgs.msg import ServosPWM_MSG, Bumpers
from r2robotics_msgs.msg import SonarArray
from std_srvs.srv import SetBool
from r2robotics_srvs.srv import move as moveSrv
from r2robotics_srvs.srv import set_direction, motors_disable, activate
from r2robotics_srvs.srv import set_position
from r2robotics_srvs.srv import set_direction
from r2robotics_srvs.srv import stopControl
from r2robotics_srvs.srv import deactivate
from r2robotics_srvs.srv import setControlMaxLinearSpeed
from r2robotics_actions.msg import TargetPointAction, TargetPointGoal
from r2robotics_actions.msg import ConsigneAction, ConsigneGoal

#########################################
### States for the main robot actions ###
#########################################

VERT_BRAS_FERME     = -30 
VERT_BRAS_OUVERT    = -90
ORANGE_BRAS_FERME   = -34
ORANGE_BRAS_OUVERT  = 26

class GR_Actuator(State):
    def __init__(self, stratmaster):
        State.__init__(self, outcomes=['succeeded','failed'], input_keys=['input_action'])
        self.stratmaster = stratmaster

    def execute(self, userdata):
        if not self.stratmaster.MAIN_ROBOT:
            return
        act = userdata.input_action.gr_actuator
        value = userdata.input_action.gr_actuator_value
        # try:
        if act == "interrupteur_arm":
            rospy.wait_for_service('move')
            self.move = rospy.ServiceProxy('move', moveSrv )
            self.set_interrupteur_arm(self.stratmaster.color, value)
            STRAT_LOG("BEE ARM SET : " + value)
        return 'succeeded'
        # except:
        #     return 'failed'

    def set_interrupteur_arm(self, color, val):
        if val == 'LOW':
            self.move(idServos=[100], angles=[ORANGE_BRAS_FERME], times=[10])
            self.move(idServos=[101], angles=[VERT_BRAS_FERME], times=[10])
            return
        elif val == 'HIGH':
            if color == Color.GREEN:
                self.move(idServos=[101], angles=[VERT_BRAS_OUVERT], times=[10])
                self.move(idServos=[100], angles=[ORANGE_BRAS_FERME], times=[10])
            if color == Color.ORANGE:
                self.move(idServos=[100], angles=[ORANGE_BRAS_OUVERT], times=[10])
                self.move(idServos=[101], angles=[VERT_BRAS_FERME], times=[10])
