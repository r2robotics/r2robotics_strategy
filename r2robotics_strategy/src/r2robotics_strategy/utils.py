#!/usr/bin/env python
import rospy

M_PI = 3.14159265358979323846

def DEG2RAD(deg):
    return deg * M_PI / 180.0

def RAD2DEG(rad):
    return rad * 180.0 / M_PI

class colors:
    WHITE = '\033[97m'
    CYAN = '\033[96m'
    MAGENTA = '\033[95m'
    BLUE = '\033[94m'
    YELLOW = '\033[93m'
    GREEN = '\033[92m'
    RED = '\033[91m'
    
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    HIGHLIGHT = '\033[7m'
    
    NORMAL = WHITE
    WARNING = YELLOW
    FAIL = RED

def LOG(msg, *col):
    if not col:
        col = colors.NORMAL;
    rospy.loginfo("".join(col) + msg + colors.ENDC)

def STRAT_LOG(msg):
    rospy.loginfo("".join(colors.GREEN) + msg + colors.ENDC)
