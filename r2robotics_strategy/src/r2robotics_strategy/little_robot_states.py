#!/usr/bin/env python

import roslib
import rospy
from smach import *
from smach_ros import SimpleActionState
from enum import Enum

from r2robotics_strategy.utils import *
from r2robotics_strategy.actions import Color

from r2robotics_msgs.msg import Movement
from r2robotics_msgs.msg import RobotPosition
from r2robotics_msgs.msg import ServosPWM_MSG, Bumpers
from r2robotics_msgs.msg import SonarArray
from std_srvs.srv import SetBool
from r2robotics_srvs.srv import move as moveSrv
from r2robotics_srvs.srv import set_direction, motors_disable, activate
from r2robotics_srvs.srv import set_position
from r2robotics_srvs.srv import set_direction
from r2robotics_srvs.srv import stopControl
from r2robotics_srvs.srv import deactivate
from r2robotics_srvs.srv import setControlMaxLinearSpeed
from r2robotics_srvs.srv import move_direction
from r2robotics_actions.msg import TargetPointAction, TargetPointGoal
from r2robotics_actions.msg import ConsigneAction, ConsigneGoal

###########################################
### States for the little robot actions ###
###########################################

# Roue Haute
RH_ID               = 12
RH_TIME             = 100
RH_OUVERTURE_0      = -120
RH_OUVERTURE_1      = 120
RH_OUVERTURE_2      = 0
RH_OUVERTURE_3      = 0
RH_OUVERTURE_4      = 0
RH_OUVERTURE_5      = 0
RH_OUVERTURE        = [RH_OUVERTURE_0, RH_OUVERTURE_1, RH_OUVERTURE_2, RH_OUVERTURE_3, RH_OUVERTURE_4, RH_OUVERTURE_5]
RH_OUVERTURE_DIR    = False

RH_FERMETURE_EVEN   = True
RH_FERMETURE_ODD    = False
RH_FERMETURE_0      = 20    # true
RH_FERMETURE_1      = -120  # false
RH_FERMETURE_2      = -100  # true
RH_FERMETURE_3      = 120   # false
RH_FERMETURE_4      = 140   # true
RH_FERMETURE_5      = 0     # false
RH_FERMETURE        = [RH_FERMETURE_0, RH_FERMETURE_1, RH_FERMETURE_2, RH_FERMETURE_3, RH_FERMETURE_4, RH_FERMETURE_5]
RH_FERMETURE_DIR    = True

# Roue Basse
RB_ID               = 11
RB_TIME             = 100 
RB_SORTIE_CANON_0   = -85
RB_SORTIE_CANON_1   = 155
RB_SORTIE_CANON_2   = 35
RB_SORTIE_CANON     = [RB_SORTIE_CANON_0, RB_SORTIE_CANON_1, RB_SORTIE_CANON_2]
RB_SORTIE_CANON_DIR = False

# Lanceur
CANON_ID          = 13
CANON_TIR         = 140
CANON_TIR_DIR     = False
CANON_PRENDRE     = -65
CANON_TIME        = 1

# Bielle
BIELLE_ID           = 10
BIELLE_DEDANS       = 140
BIELLE_PUSHPUSH     = 0
BIELLE_TIME         = 10

RightBeeArmStatus = {
    'INSIDE' : 210,
    'HORIZONTAL' : 136,
    'HIGH' : 60
}

LeftBeeArmStatus = {
    'INSIDE' : 4,
    'HORIZONTAL' : 80,
    'HIGH' : 150
}


class PMI_SetBeeArm(State):
    def __init__(self, stratmaster):
        State.__init__(self, outcomes=['succeeded','failed'], input_keys=['input_action'])
        self.stratmaster = stratmaster
        self.servos_pub = rospy.Publisher('servos_PWM', ServosPWM_MSG, queue_size=10)

    def execute(self, userdata):
        value = userdata.input_action.bee_arm_value
        try:
            if self.stratmaster.color == Color.GREEN:
                self.servos_pub.publish( bras_abeille_gauche=LeftBeeArmStatus['INSIDE'],
                                        bras_abeille_droite=RightBeeArmStatus[value] )
            if self.stratmaster.color == Color.ORANGE:
                self.servos_pub.publish( bras_abeille_gauche=LeftBeeArmStatus[value],
                                        bras_abeille_droite=RightBeeArmStatus['INSIDE'] )
            STRAT_LOG("BEE ARM SET : " + value)
            return 'succeeded'
        except rospy.ROSInterruptException:
            return 'failed'


class PMI_Roue(State):
    def __init__(self, stratmaster):
        State.__init__(self, outcomes=['succeeded','failed'], input_keys=['input_action'])
        self.stratmaster = stratmaster
        rospy.wait_for_service('/act_moveDirection')
        self.moveDir = rospy.ServiceProxy('/act_moveDirection', move_direction )

    def execute(self, userdata):
        STRAT_LOG("Setting PMI roue : " + userdata.input_action.roue + " - " + userdata.input_action.roue_sens + " : " + str(userdata.input_action.roue_iteration) +" ...")
        i = userdata.input_action.roue_iteration

        try:
            if userdata.input_action.roue == "ROUE_HAUTE":
                if userdata.input_action.roue_sens == "OUVERTURE":
                    self.moveDir(idServo=RH_ID,
                                        angle=RH_OUVERTURE[i],
                                        direction=RH_OUVERTURE_DIR,
                                        time=RH_TIME)
                elif userdata.input_action.roue_sens == "FERMETURE":
                    if i % 2 == 0:
                        direction = RH_FERMETURE_EVEN
                    else:
                        direction = RH_FERMETURE_ODD
                    self.moveDir(idServo=RH_ID,
                                        angle=RH_FERMETURE[i],
                                        direction=direction,
                                        time=RH_TIME)
                    STRAT_LOG("test roue haute fermeture ??")

            elif userdata.input_action.roue == "ROUE_BASSE":
                if userdata.input_action.roue_sens == "CANON":
                    self.moveDir(idServo=RB_ID,
                                        angle=RB_SORTIE_CANON[i],
                                        direction=RB_SORTIE_CANON_DIR,
                                        time=RB_TIME)
            STRAT_LOG("PMI roue : " + userdata.input_action.roue + " - " + userdata.input_action.roue_sens + " : "+ str(i) )
            return 'succeeded'
        except:
            return 'failed'


class PMI_PushPush(State):
    def __init__(self, stratmaster):
        State.__init__(self, outcomes=['succeeded','failed'], input_keys=['input_action'])
        self.stratmaster = stratmaster
        rospy.wait_for_service('/act_move')
        self.move = rospy.ServiceProxy('/act_move', moveSrv )

    def execute(self, userdata):
        # value = self.stratmaster.bee_arm_value # TODO
        try:
            if userdata.input_action.bielle_position == "DEDANS":
                self.move(idServos=[BIELLE_ID],
                                    angles=[BIELLE_DEDANS],
                                    times=[RB_TIME])
            if userdata.input_action.bielle_position == "PUSHPUSH":
                self.move(idServos=[BIELLE_ID],
                                    angles=[BIELLE_PUSHPUSH],
                                    times=[BIELLE_TIME])
            STRAT_LOG("PMI bielle : " + userdata.input_action.bielle_position )
            return 'succeeded'
        except rospy.ROSInterruptException:
            return 'failed'


class PMI_Canon(State):
    def __init__(self, stratmaster):
        State.__init__(self, outcomes=['succeeded','failed'], input_keys=['input_action'])
        self.stratmaster = stratmaster
        rospy.wait_for_service('act_moveDirection')
        rospy.wait_for_service('act_move')
        self.move = rospy.ServiceProxy('act_move', moveSrv )
        self.set_direction = rospy.ServiceProxy('act_moveDirection', move_direction )

    def execute(self, userdata):
        # value = self.stratmaster.bee_arm_value # TODO
        try:
            if userdata.input_action.canon_position == "TIR":
                self.set_direction(idServo=CANON_ID,
                                    angle=CANON_TIR,
                                    direction=CANON_TIR_DIR,
                                    time=CANON_TIME)
            if userdata.input_action.canon_position == "PRENDRE_BALLE":
                self.move(idServos=[CANON_ID],
                            angles=[CANON_PRENDRE],
                            times=[CANON_TIME])
            STRAT_LOG("PMI canon : " + userdata.input_action.canon_position )
            return 'succeeded'
        except rospy.ROSInterruptException:
            return 'failed'