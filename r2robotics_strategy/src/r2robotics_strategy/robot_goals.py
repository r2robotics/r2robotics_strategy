#!/usr/bin/env python

import roslib
import rospy
from r2robotics_strategy.point import Point
from enum import Enum

from r2robotics_strategy.actions import *

SLOW_SPEED = 300
NORMAL_SPEED = 700
HIGH_SPEED = 1000

class Strat(Enum):
    HOMOLOGATION_AVOIDANCE = 0
    HOMOLOGATION = 1
    QUALIF = 2
    FINALE = 3
    TEST_TABLE = 4
    DEMO = 5
    HOMOLOGATION_ABEILLE = 6
    HOMOLOGATION_DISTRIBUTEUR = 7

class RobotGoals(object):
    def __init__(self, color=None, strat_type=None):
        if color is not None:
            self.set_color(color)
        if strat_type is not None:
            self.set_strat(strat_type)

    def set_color(self, color):
        self.color = color

    def set_strat(self, strat_type):
        self.type = strat_type
        self.actions = ActionList()
        if self.type == Strat.HOMOLOGATION_AVOIDANCE:
            self.init_homolo_avoidance()
        elif self.type == Strat.HOMOLOGATION:
            self.init_homologation()
        elif self.type == Strat.QUALIF:
            self.init_qualif()
        elif self.type == Strat.FINALE:
            self.init_finale()
        elif self.type == Strat.DEMO:
            self.init_demo()
        elif self.type == Strat.HOMOLOGATION_ABEILLE:
            self.init_homolo_abeille()
        elif self.type == Strat.HOMOLOGATION_DISTRIBUTEUR:
            self.init_homolo_distributeur()

    def is_empty(self):
        return (self.actions.size() == 0 )

    def x(self, _x):
        return _x

    def y(self, _y):
        if self.color == Color.GREEN:
            return _y;
        elif self.color == Color.ORANGE:
            return 3000-_y;

    def cap(self, _cap):
        if self.color == Color.GREEN:
            return _cap;
        elif self.color == Color.ORANGE:
            return -_cap;

    def add(self, ac):
        self.actions.append(ac)

    def add_goto(self, pointID, cap_needed = None, speed = None ):
        self.actions.add_goto( pointID, cap_needed )

    def add_turn(self, _cap):
        self.actions.add_turn(self.cap(_cap))

    def add_wait(self, wait_time_seconds):
        self.actions.add_wait(wait_time_seconds)

    def add_recalibration(self, dx, dy):
        self.actions.add_recalibration(dx, dy)

    def add_stop_avoidance(self):
        self.actions.add_stop_avoidance()

    def add_gr_actuator(self, actuator, value):
        self.actions.add_gr_actuator(actuator, value)

    def add_resume_avoidance(self):
        self.actions.add_resume_avoidance()

    def add_PMI_roue(self, roue, sens, it):
        self.actions.add_PMI_roue(roue, sens, it)

    def add_PMI_pushpush(self, pos):
        self.actions.add_PMI_pushpush(pos)

    def add_PMI_canon(self, pos):
        self.actions.add_PMI_canon(pos)

    def get_next_action(self):
        return self.actions.get_next_action()

    def pop(self, i):
        if i == 0:
            return self.pop_first()

    def pop_first(self):
        return self.actions.pop_first()