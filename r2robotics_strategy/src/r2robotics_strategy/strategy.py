#!/usr/bin/env python

import roslib
import rospy
from smach import *
from smach_ros import SimpleActionState, MonitorState, ServiceState
from smach import StateMachine

from r2robotics_strategy.states import *
from r2robotics_strategy.little_robot_states import *
from r2robotics_strategy.main_robot_states import *
from r2robotics_strategy.utils import *
from r2robotics_strategy.actions import *
from r2robotics_strategy.robot_goals import *

from std_msgs.msg import Empty
from std_msgs.msg import String
from r2robotics_msgs.msg import Movement
from r2robotics_msgs.msg import RobotPosition
from r2robotics_msgs.msg import Bumpers

from std_srvs.srv import Trigger
from r2robotics_srvs.srv import triggerAvoidance
from r2robotics_srvs.srv import set_position
from r2robotics_srvs.srv import resumeControl
from r2robotics_srvs.srv import startRecalibration
# from r2robotics_srvs.srv import move as moveSrv
# from r2robotics_srvs.srv import set_direction, motors_disable, activate

from r2robotics_actions.msg import TargetPointAction, TargetPointGoal
from r2robotics_actions.msg import ConsigneAction, ConsigneGoal

USE_POSITION = True
MATCH_DURATION = 100 # seconds

if USE_POSITION:
    from r2robotics_msgs.msg import Position

class StrategyMaster(object):
    def __init__(self):
        rospy.init_node( "strategy_node" )
        STRAT_LOG( "strategy_node started !" )

        self.MAIN_ROBOT = rospy.get_param("main_robot", False)
        self.LITTLE_ROBOT = rospy.get_param("little_robot", False)

        if self.MAIN_ROBOT:
            STRAT_LOG( "MAIN ROBOT" )
            import r2robotics_strategy.main_robot_goals

        elif self.LITTLE_ROBOT:
            STRAT_LOG( "LITTLE ROBOT" )
            import r2robotics_strategy.little_robot_goals
        self.goals = RobotGoals()

        self.table_state = TableState()
        self.next_action = Action()

        # wait for services to be called later in callbacks
        self.init_services()
        self.init_act()
        # init various variables
        self.init_variables()

        # wait for bumper feedback and init color and strat
        self.init_color()
        # self.init_strat("HOMOLOGATION")
        self.init_strat("QUALIF") # TODO pour changer de strategie

        self.init_start_position()
        self.init_max_speeds()

    def init_max_speeds(self):
        if self.type == Strat.HOMOLOGATION:
            self.max_linear_speed = 0.3
        elif not self.LITTLE_ROBOT:
            self.max_linear_speed = 0.3
        else:
            self.max_linear_speed = 0.5
        STRAT_LOG( "Max speed set" )

    def init_start_position(self):
        if self.color == Color.ORANGE :
            self.start_x = rospy.get_param("x_init_orange", 215)
            self.start_y = rospy.get_param("y_init_orange", 2755)
            self.start_cap = rospy.get_param("yaw_init_orange", -29.18)
        else:
            self.start_x = rospy.get_param("x_init_vert", 215)
            self.start_y = rospy.get_param("y_init_vert", 245)
            self.start_cap = rospy.get_param("yaw_init_vert", 29.18)
        self.set_start_position()
        STRAT_LOG( "Start position set" )

    def init_variables(self):
        self.current_action = Action()
        self.current_x = 0
        self.current_y = 0
        self.current_cap = 0
        self.speed_x = 0
        self.speed_y = 0
        self.nb_positions_for_speed = 10
        self.old_x = [0]
        self.old_y = [0]

    def init_color(self):
        bump = rospy.wait_for_message('bumpers_feedback', Bumpers)
        if bump.bumpers[1] is False:
            STRAT_LOG( "COLOR GREEN" )
            self.color = Color.GREEN
        elif bump.bumpers[1] is True:
            STRAT_LOG( "COLOR ORANGE" )
            self.color = Color.ORANGE
        self.goals.set_color(self.color)
        STRAT_LOG( "Color set" )

    def init_strat(self, strat_string=None):
        if strat_string is None:
            strat_string = "HOMOLOGATION"
        # Set STRAT
        if strat_string == "HOMOLOGATION":
            self.type = Strat.HOMOLOGATION
            STRAT_LOG( "Strat set : HOMOLOGATION" )
        if strat_string == "QUALIF":
            self.type = Strat.QUALIF
            STRAT_LOG( "Strat set : QUALIF" )
        if strat_string == "HOMOLOGATION_ABEILLE":
            self.type = Strat.HOMOLOGATION_ABEILLE
        if strat_string == "HOMOLOGATION_DISTRIBUTEUR":
            self.type = Strat.HOMOLOGATION_DISTRIBUTEUR
        self.goals.set_strat(self.type)

    def init_services(self):
        self.timeout_pub = rospy.Publisher('match_timeout', Empty, queue_size=10)
        rospy.wait_for_service('resume_control')
        self.resume_control_srv = rospy.ServiceProxy('resume_control', resumeControl )
        rospy.wait_for_service('start_recalibration')
        self.recalibration_srv = rospy.ServiceProxy('start_recalibration', startRecalibration )
        if USE_POSITION:
            rospy.Subscriber( 'odometry_feedback', Position, self.position_callback )
        rospy.Subscriber("set_strategy", String, self.set_strategy_CB)

    def set_strategy_CB(self, data):
        self.init_strat(data.data)

    def init_act(self):
        self.servos_pub = rospy.Publisher('servos_PWM', ServosPWM_MSG, queue_size=10)
        self.servos_pub.publish( bras_abeille_gauche=LeftBeeArmStatus['INSIDE'],
                                 bras_abeille_droite=RightBeeArmStatus['INSIDE'] )

    def set_start_position(self):
        set_pos = rospy.ServiceProxy('trajectory_set_position', set_position)
        res = set_pos(x=self.start_x, y=self.start_y, theta=self.start_cap)

    def position_callback(self, data):
        self.current_x = data.x
        self.current_y = data.y
        self.current_cap = RAD2DEG( data.theta )
        # update speed (no division so it's not exactly speed value, its distance made in a certain number of iteration)
        self.speed_x = self.current_x - self.old_x[0]
        self.speed_y = self.current_y - self.old_y[0]
        self.old_x.append(self.current_x)
        self.old_y.append(self.current_y)
        if len(self.old_x) > self.nb_positions_for_speed:
            self.old_x.pop(0)
        if len(self.old_y) > self.nb_positions_for_speed:
            self.old_y.pop(0)


    # whatever message we receive for the timer monitor, we terminate
    def monitor_cb(self, ud, msg):
        return False

    def start_monitor_prepare_cb(self, ud, msg):
        return not msg.bumpers[0] # Depends on bumper connection

    def start_monitor_ready_cb(self, ud, msg):
        return msg.bumpers[0] # Depends on bumper connection

    def avoidance_cb(self, ud, msg):
        return True
        # if self.deactivate_collision:
        #     return True

        # col = must_stop_static(msg.distances, self.current_x, self.current_y, self.current_cap, self.speed_x, self.speed_y, self.simple_avoidance)
        # if col[0]:
        #     return False
        #     #return True
        # else:
        #     return True # TODO heu, c'etait pas l'inverse ? ><


    # gets called when either TIMER or MATCH_AVOIDANCE terminates
    def child_term_cb(self, outcome_map):
        print '\n\n', outcome_map, '\n\n'
        if outcome_map['TIMER'] == 'invalid':
        #if outcome_map['MATCH_AVOIDANCE_MACHINE'] is None:
            # terminate only if the timer is over
            return True
        return False

    def child_term_cb_avoidance(self, outcome_map):
        return True

    # gets called when both AVOIDANCE and MATCH terminate
    def blocage_out_cb(self, outcome_map):
        if outcome_map['BLOCAGE_DETECTION'] == 'blocage_detected':
            return 'blocage_detected'
        if outcome_map['GOTO_AS_LONG_AS_NO_BLOCAGE'] == 'succeeded':
            return 'succeeded'
        if outcome_map['GOTO_AS_LONG_AS_NO_BLOCAGE'] == 'aborted':
            return 'aborted'
        else:
            return 'succeeded'

    # gets called when both AVOIDANCE and MATCH terminate
    def match_out_cb(self, outcome_map):
        print '\n\n', outcome_map, '\n\n'
        # if outcome_map['AVOIDANCE'] == 'invalid':
        #     return 'collision_detected'
        if outcome_map['MATCH'] is None:
            return 'match_preempted'
        if outcome_map['MATCH'] == 'match_done':
            return 'match_done'
        if outcome_map['MATCH'] == 'match_failed':
            return 'match_failed'
        if outcome_map['MATCH'] == 'match_preempted':
            return 'match_preempted'
        else:
            return 'match_done'

    # gets called when both TIMER and MATCH_AVOIDANCE_MACHINE terminate
    def timer_out_cb(self, outcome_map):
        if outcome_map['MATCH_AVOIDANCE_MACHINE'] == 'match_done':
            return 'match_done'
        if outcome_map['MATCH_AVOIDANCE_MACHINE'] == 'match_failed':
            return 'match_failed'
        if outcome_map['MATCH_AVOIDANCE_MACHINE'] == 'match_preempted':
            return 'timeout'
        else:
            return 'match_done'

    # Callback for SimpleActionState that implemente actions
    def action_goal_cb( self, userdata, goal ):
        self.resume_control_srv(resume_last_command=True)

        if userdata.input_action.action == 'move':
            _goal = ConsigneGoal()
            _goal.command.distance = _distance
            _goal.command.orientation = _orientation
            _goal.command.self_rotation = _self_rotation
            _goal.command.speed = _speed
        elif userdata.input_action.action == 'goto':
            STRAT_LOG("GOTO : point " + userdata.input_action.pointID)
            _goal = TargetPointGoal()
            _goal.pointID = userdata.input_action.pointID
            _goal.self_rotation_only = userdata.input_action.self_rotation_only
            _goal.mode = 1
            # _goal.self_rotatio n_only = userdata.input_action.pose
        else:
            return
        return _goal


    @cb_interface( input_keys=[], output_keys=[],
                        outcomes=['done'])
    def adjust_traj_cb( userdata, tb_goals, curr_action ):
        if curr_action.action == 'goto':
            tb_goals.add(curr_action)
        return 'done'

    @cb_interface( input_keys=[], output_keys=[],
                        outcomes=['done'])
    def remove_action_cb( userdata, tb_goals ):
        tb_goals.pop(0)
        return 'done'

    @cb_interface( input_keys=['action_wait'], output_keys=[],
                        outcomes=['done'])
    def wait_cb( userdata ):
        if userdata.action_wait.wait_time_seconds > 0:
            rospy.sleep(userdata.action_wait.wait_time_seconds)
        return 'done'

    @cb_interface( input_keys=['action_recalibration'], output_keys=[],
                        outcomes=['succeeded'])
    def recalibration_cb( userdata ):
        if userdata.action_recalibration.dx > 0 and userdata.action_recalibration.dy > 0:
            self.recalibration_srv(x=userdata.action_recalibration.dx, y=userdata.action_recalibration.dy)
        return 'succeeded'

    # Decision function : main strat is here
    @cb_interface( input_keys=[], output_keys=['next_action'],
                        outcomes=['decision_do_nothing', 'decision_do_move',
                                    'decision_do_goto', 'decision_do_wait',
                                    'decision_do_recalibration', 'decision_do_PMI_set_bee_arm',
                                    'decision_do_PMI_roue', 'decision_do_PMI_pushpush',
                                    'decision_do_stop_avoidance', 'decision_do_resume_avoidance',
                                    'decision_do_gr_actuator',
                                    'decision_do_PMI_canon', 'decision_failed', 'preempted'])
    def decision_cb( userdata, tb_goals, tb_state, strat_type, curr_action ):
        #rospy.sleep(0.5) #USEFUL ??? TODO !!!
        if not tb_goals.is_empty():
            nx_action = tb_goals.get_next_action()
            userdata.next_action = nx_action
            curr_action.copy(nx_action)
            return 'decision_do_' + nx_action.action
        else:
            nx_action = Action()
            nx_action.set_donothing()
            userdata.next_action = nx_action
            return 'decision_do_nothing'


    # Function called when entered in state START_TIMER
    @cb_interface( input_keys=['cb'], output_keys=[],
                    outcomes=['succeeded', 'failed'] )
    def start_timer( userdata, cb, start_x, start_y, start_cap):
        rospy.Timer( rospy.Duration(MATCH_DURATION), cb )
        # set_pos = rospy.ServiceProxy('trajectory_set_position', set_position)
        # res = set_pos(x=start_x, y=start_y, theta=start_cap)
        STRAT_LOG("starting timer !!! ...")
        return 'succeeded'

    def timout_cb( self, event ):
        self.timeout_pub.publish( Empty() )

    def start_smachine(self):
        STRAT_LOG("STARTING STATE MACHINE ......\n")

################################################################################
#                         START CREATION OF STATE MACHINE                      #
################################################################################

        #########################################################
        ### CREATE BLOCAGE DETECTED CONCURRENCE STATE MACHINE ###
        #########################################################

        # creating the concurrence state machine
        # goto_concurrence = Concurrence(outcomes=['succeeded', 'blocage_detected', 'preempted', 'aborted'],
        #                 default_outcome='succeeded',
        #                 input_keys=['input_action'],
        #                 child_termination_cb = self.child_term_cb_avoidance,
        #                 outcome_cb = self.blocage_out_cb)
                        
        # with goto_concurrence:
        #     Concurrence.add( 'GOTO_AS_LONG_AS_NO_BLOCAGE',
        #                         SimpleActionState('action_trajectory',
        #                                         TargetPointAction,
        #                                         goal_cb=self.action_goal_cb,
        #                                         input_keys=['input_action']),)
        #     Concurrence.add('BLOCAGE_DETECTION',
        #                     MonitorState("blocking_detected", Empty, self.monitor_cb) )


        ######################################
        ### CREATE MATCH SUB STATE MACHINE ###
        ######################################

        sm_match = StateMachine( outcomes=['match_done', 'match_failed', 'match_preempted'] )

        # Fill match sub state machine
        with sm_match:

            StateMachine.add( 'NOMOVE', Nothing(),
                               transitions={'continue':'DECISION'} )

            """
            StateMachine.add( 'ADJUST_TRAJECTORY',
                                CBState(self.adjust_traj_cb, cb_args=[self.goals, self.current_action]),
                                transitions={'done':'DECISION'},
                                remapping={'old_action':'old_action'} )
                                """

            StateMachine.add( 'REMOVE_ACTION_DONE',
                                CBState(self.remove_action_cb, cb_args=[self.goals]),
                                transitions={'done':'DECISION'})


            StateMachine.add( 'DECISION',
                                CBState(self.decision_cb, cb_args=[self.goals, self.table_state, self.type, self.current_action]),
                                transitions={
                                    'decision_do_nothing':'NOTHING',
                                    'decision_do_move':'MOVE',
                                    'decision_do_goto':'GOTO',
                                    'decision_do_wait':'WAIT',
                                    'decision_do_recalibration':'START_RECALIBRATION',
                                    'decision_do_PMI_set_bee_arm':'PMI_SET_BEE_ARM',
                                    'decision_do_PMI_roue':'PMI_ROUE',
                                    'decision_do_PMI_pushpush':'PMI_PUSHPUSH',
                                    'decision_do_PMI_canon':'PMI_CANON',
                                    'decision_do_stop_avoidance':'STOP_AVOIDANCE',
                                    'decision_do_gr_actuator':'GR_ACTUATOR',
                                    'decision_do_resume_avoidance':'RESUME_AVOIDANCE',
                                    'decision_failed':'match_failed',
                                    'preempted':'match_preempted'},
                                remapping={'next_action':'next_action'} )

            StateMachine.add( 'NOTHING', Wait(),
                                transitions={'continue':'NOTHING', 'preempted':'match_preempted'} )

            StateMachine.add( 'PMI_SET_BEE_ARM', PMI_SetBeeArm(self),
                                transitions={'succeeded':'REMOVE_ACTION_DONE', 'failed':'DECISION'},
                                remapping={'input_action':'next_action'}  )

            StateMachine.add( 'PMI_ROUE', PMI_Roue(self),
                                transitions={'succeeded':'REMOVE_ACTION_DONE', 'failed':'DECISION'},
                                remapping={'input_action':'next_action'}  )

            StateMachine.add( 'PMI_PUSHPUSH', PMI_PushPush(self),
                                transitions={'succeeded':'REMOVE_ACTION_DONE', 'failed':'DECISION'},
                                remapping={'input_action':'next_action'}  )

            StateMachine.add( 'PMI_CANON', PMI_Canon(self),
                                transitions={'succeeded':'REMOVE_ACTION_DONE', 'failed':'DECISION'},
                                remapping={'input_action':'next_action'}  )

            StateMachine.add( 'WAIT',
                                CBState(self.wait_cb),
                                transitions={'done':'REMOVE_ACTION_DONE'},
                                remapping={'action_wait':'next_action'}  )

            StateMachine.add( 'MOVE',
                                SimpleActionState('action_consigne',
                                                ConsigneAction,
                                                goal_cb=self.action_goal_cb,
                                                input_keys=['input_action']),
                                transitions={'succeeded':'DECISION', 'preempted':'match_preempted',
                                            'aborted':'DECISION'},
                                remapping={'input_action':'next_action'} )

            StateMachine.add( 'GOTO',
                                SimpleActionState('action_trajectory',
                                                TargetPointAction,
                                                goal_cb=self.action_goal_cb,
                                                input_keys=['input_action']),
                                transitions={'succeeded':'REMOVE_ACTION_DONE', 'preempted':'match_preempted',
                                            'aborted':'DECISION'},
                                remapping={'input_action':'next_action'} )

            # StateMachine.add( 'GOTO',
            #                     goto_concurrence,
            #                     transitions={'succeeded':'REMOVE_ACTION_DONE', 'preempted':'match_preempted',
            #                                 'aborted':'DECISION', 'blocage_detected':'REMOVE_ACTION_DONE'},
            #                     remapping={'input_action':'next_action'} )

            StateMachine.add( 'START_RECALIBRATION',
                                CBState(self.recalibration_cb),
                                transitions={'succeeded':'WAIT_FOR_RECALIBRATION'},
                                remapping={'action_wait':'next_action'}  )

            StateMachine.add( 'WAIT_FOR_RECALIBRATION',
                                MonitorState("blocking_detected", Empty, self.monitor_cb),
                                transitions={'invalid':'REMOVE_ACTION_DONE', 'valid':'DECISION', 'preempted':'match_preempted' } )

            StateMachine.add( 'STOP_AVOIDANCE',
                                ServiceState("avoidance_stop", Trigger),
                                transitions={'succeeded':'REMOVE_ACTION_DONE', 'aborted':'REMOVE_ACTION_DONE', 'preempted':'match_preempted' } )

            StateMachine.add( 'RESUME_AVOIDANCE',
                                ServiceState("avoidance_resume", Trigger),
                                transitions={'succeeded':'REMOVE_ACTION_DONE', 'aborted':'REMOVE_ACTION_DONE', 'preempted':'match_preempted' } )

            StateMachine.add( 'GR_ACTUATOR', GR_Actuator(self),
                                transitions={'succeeded':'REMOVE_ACTION_DONE', 'failed':'REMOVE_ACTION_DONE'},
                                remapping={'input_action':'next_action'}  )

            # StateMachine.add( 'SET_SPEED', 
            #                     SetLinearSpeed(self.max_linear_speed),
            #                     transitions={'succeeded':'REMOVE_ACTION_DONE', 'aborted':'DECISION' })


        #################################################
        ### CREATE MATCH/TIMER CONCURRENCE SUBMACHINE ###
        #################################################

        match_concurrence = Concurrence( outcomes=[ 'match_done', 'match_failed', 'match_preempted', 'collision_detected' ],
                                        default_outcome='match_failed',
                                        child_termination_cb=self.child_term_cb_avoidance,
                                        outcome_cb=self.match_out_cb )

        with match_concurrence:
            # Concurrence.add( 'AVOIDANCE')
            # Concurrence.add( 'AVOIDANCE', MonitorState("/triggerAvoidance", triggerAvoidance, self.avoidance_cb) )
            Concurrence.add( 'MATCH', sm_match)

        sm_avoidance = StateMachine( outcomes=['match_done', 'match_failed', 'match_preempted'])

        with sm_avoidance:
            StateMachine.add( 'MATCH_AVOIDANCE', match_concurrence,
                            transitions={'match_done':'match_done',
                                        'match_failed':'match_failed',
                                        'match_preempted':'match_preempted',
                                        'collision_detected':'AVOIDANCE_STOP'} )
                                        #'collision_detected':'AVOIDANCE_STOP'} )
            StateMachine.add( 'AVOIDANCE_STOP', StopAndDisable(self),
                                transitions={'done':'MATCH_AVOIDANCE', 'preempted':'match_preempted'})
            #StateMachine.add( 'WAIT_END', Nothing(),
            #            transitions={'continue':'WAIT_END', 'preempted':'match_preempted'})


        ######################################
        ### CREATE MATCH/TIMER CONCURRENCE ###
        ######################################

        # Create a concurrence of the match sub machine and a monitor state
        # to check if the timer has reached the end of the match (100s)

        timer_concurrence = Concurrence( outcomes=['timeout', 'match_failed', 'match_done'],
                                        default_outcome='timeout',
                                        child_termination_cb=self.child_term_cb,
                                        outcome_cb=self.timer_out_cb )

        with timer_concurrence:
            Concurrence.add( 'TIMER', MonitorState("/match_timeout", Empty, self.monitor_cb) )
            Concurrence.add( 'MATCH_AVOIDANCE_MACHINE', sm_avoidance)



        #####################################
        ### CREATE INIT SEQUENCE SMACHINE ###
        #####################################

        init_sequence = Sequence(
            outcomes = ['succeeded', 'aborted', 'preempted'],
                        connector_outcome='succeeded' )

        with init_sequence:
            Sequence.add( 'INIT_WAIT', InitWaitState(self.start_x, self.start_y, self.start_cap) )
            Sequence.add( 'TEST_ACTUATORS', TestActuators(self) )
            Sequence.add( 'SET_SPEED', SetLinearSpeed(self.max_linear_speed) )


        ###################################
        ### CREATE GLOBAL STATE MACHINE ###
        ###################################

        # Create global state machine
        sm = StateMachine( outcomes=['MATCH_OVER', 'FAILURE'] )

        # Fill global state machine
        with sm:

            StateMachine.add( 'INITIALIZATION', init_sequence,
                               transitions={'succeeded':'PREPARE', 'aborted':'FAILURE', 'preempted':'FAILURE'} )

            StateMachine.add( 'PREPARE', MonitorState("/bumpers_feedback", Bumpers, self.start_monitor_prepare_cb),
                                transitions={'invalid':'INIT_CONTROL', 'valid':'PREPARE', 'preempted':'INIT_CONTROL'} )

            StateMachine.add( 'INIT_CONTROL', SendInitialControl(self, self.start_x, self.start_y),
                               transitions={'succeeded':'READY', 'aborted':'READY', 'preempted':'READY'} )

            StateMachine.add( 'READY', MonitorState("/bumpers_feedback", Bumpers, self.start_monitor_ready_cb),
                                transitions={'invalid':'START_TIMER', 'valid':'READY', 'preempted':'READY'} )

            StateMachine.add( 'START_TIMER', CBState(self.start_timer, cb_args=[self.timout_cb, self.start_x, self.start_y, self.start_cap], cb_kwargs={}),
                               transitions={'succeeded':'SET_STRAT_AND_COLOR', 'failed':'FAILURE'} )

            StateMachine.add( 'SET_STRAT_AND_COLOR', SetStratAndColor(self),
                               transitions={'succeeded':'INIT_ACTUATORS', 'failed':'FAILURE'} )

            StateMachine.add( 'INIT_ACTUATORS', InitActuators(self),
                               transitions={'succeeded':'MATCH_TIMER', 'failed':'MATCH_TIMER'} )

            StateMachine.add( 'MATCH_TIMER', timer_concurrence,
                               transitions={'match_done':'STOP',
                               'timeout':'STOP', 'match_failed':'FAILURE'} )

            StateMachine.add( 'STOP', StopAndDisable(self),
                               transitions={'done':'END', 'preempted':'END'} )
                               
            StateMachine.add( 'END', End(),
                               transitions={'end_done':'MATCH_OVER', 'end_failed':'FAILURE'} )


################################################################################
#                         STATE MACHINE CREATION FINISHED                      #
################################################################################

        STRAT_LOG("execute STATE MACHINE")

        sm.execute()
        rospy.signal_shutdown('All done.')
        # rospy.spin()